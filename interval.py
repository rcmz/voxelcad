import math
from ambiguous import *

class Interval:

    def __init__(self, a, b = None):
        self.a = float(a)
        self.b = float(b) if b is not None else self.a
        assert self.a <= self.b, str(self.a) + " > " + str(self.b)

    def __repr__(self):
        if self.a == self.b: return "[" + str(self.a) + "]"
        return "[" + str(self.a) + "," + str(self.b) + "]"

    def __neg__(self):
        return Interval(-other.b, -other.a)

    def __abs__(self):
        if self.a >= 0: return self
        if self.b < 0: return Interval(abs(self.b), abs(self.a))
        return Interval(0, max(abs(self.a), abs(self.b)))

    def __add__(self, other):
        if not isinstance(other, Interval): other = Interval(other)
        return Interval(self.a + other.a, self.b + other.b)

    def __radd__(self, other):
        return self + other
    
    def __sub__(self, other):
        if not isinstance(other, Interval): other = Interval(other)
        return Interval(self.a - other.b, self.b - other.a)

    def __rsub__(self, other):
        return  Interval(other) - self

    def __mul__(self, other):
        if not isinstance(other, Interval): other = Interval(other)
        c = [self.a*other.a, self.a*other.b, self.b*other.a, self.b*other.b]
        return Interval(min(c), max(c))

    def __rmul__(self, other):
        return self * other

    def __pow__(self, other):
        if isinstance(other, int) and other >= 0:
            if other % 2 == 1 or self.a >= 0: return Interval(self.a**other, self.b**other)
            if self.b < 0: return Interval(self.b**other, self.a**other)
            return Interval(0, max(self.a**other, self.b**other))
        if isinstance(other, float) and other >= 0:
            return math.e**(other * self.log())

    def __rpow__(self, other):
        print(type(other))
        assert isinstance(other, (int, float)) and other > 1
        return Interval(other**self.a, other**self.b)

    def log(self): 
        assert self.a > 0
        return Interval(math.log(self.a), math.log(self.b))

    def __lt__(self, other):
        if not isinstance(other, Interval): other = Interval(other)
        if self.b < other.a: return True
        if self.a > other.b: return False
        return Ambiguous

    def __gt__(self, other):
        if not isinstance(other, Interval): other = Interval(other)
        if self.a > other.b: return True
        if self.b < other.a: return False
        return Ambiguous

    def __le__(self, other):
        if not isinstance(other, Interval): other = Interval(other)
        if self.b <= other.a: return True
        if self.a >= other.b: return False
        return Ambiguous

    def __ge__(self, other):
        if not isinstance(other, Interval): other = Interval(other)
        if self.a >= other.b: return True
        if self.b <= other.a: return False
        return Ambiguous

def sqrt(x):
    if isinstance(x, Interval): return Interval(math.sqrt(x.a), math.sqrt(x.b))
    return math.sqrt(x)
