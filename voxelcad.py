import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, Pango

from render_pane import RenderPane

from importlib import reload
import cad_script

class VortexCadWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title = "Vortex CAD")
        self.set_default_size(1600, 800)

        self.header_bar = MainHeaderBar()
        self.set_titlebar(self.header_bar)

        self.paned = Gtk.HPaned()
        self.add(self.paned)

        self.text_view = TextViewPane()
        self.paned.pack1(self.text_view, False, False)

        render_area = [-2,-2,2,2]
        resolution = 4/800
        expression = cad_script.shape.f
        self.render_pane = RenderPane(render_area, expression, resolution)
        self.paned.pack2(self.render_pane, True, False)

        self.connect("destroy", Gtk.main_quit)
        self.header_bar.render_button.connect("clicked", self.render)

        self.show_all()

    def render(self, widget = None):
        text_buffer = self.text_view.get_buffer()
        text = text_buffer.get_text(*text_buffer.get_bounds(), False)

        with open("cad_script.py", "w") as cad_script_file:
            cad_script_file.write(text)
        
        reload(cad_script)
        self.render_pane.expression = cad_script.shape.f
        self.render_pane.render()

class MainHeaderBar(Gtk.HeaderBar):

    def __init__(self):
        Gtk.HeaderBar.__init__(self)

        self.set_show_close_button(True)
        self.set_has_subtitle(False)
        self.set_title("Voxel CAD")

        self.render_button = Gtk.Button()
        self.render_button.set_label("Render")
        self.pack_start(self.render_button)

class TextViewPane(Gtk.TextView):

    def __init__(self):
        Gtk.TextView.__init__(self)

        margin = 16
        self.set_top_margin(margin)
        self.set_bottom_margin(margin)
        self.set_left_margin(margin)
        self.set_right_margin(margin)

        self.set_monospace(True)
        self.override_font(Pango.FontDescription("Monaco 12"))
        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0,0,0))
        self.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1,1,1))

        self.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        self.set_size_request(800, -1)

        with open("cad_script.py", "r") as cad_script_file:
            self.get_buffer().set_text(cad_script_file.read())

window = VortexCadWindow()
Gtk.main()
