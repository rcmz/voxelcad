class AmbiguousType:

    def __repr__(self): return "Ambiguous"

    def __and__(self, other):
        if other is True: return Ambiguous
        if other is False: return False
        if other is Ambiguous: return Ambiguous
        raise TypeError

    def __rand__(self, other): return self & other

    def __or__(self, other):
        if other is True: return True
        if other is False: return Ambiguous
        if other is Ambiguous: return Ambiguous
        raise TypeError

    def __ror__(self, other): return self | other

    def __xor__(self, other):
        if other is True: return Ambiguous
        if other is False: return Ambiguous
        if other is Ambiguous: return Ambiguous
        raise TypeError

    def __rxor__(self, other): return self ^ other


Ambiguous = AmbiguousType()
