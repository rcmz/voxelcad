from ambiguous import Ambiguous
from types import MethodType
from math import *

def default_f(x, y): return False

class Shape:

    def __init__(self, f = default_f):
        self.f = f
    
    def __and__(self, other):
        def f(x, y): return self.f(x, y) & other.f(x, y)
        return Shape(f)

    def __or__(self, other):
        def f(x, y): return self.f(x, y) | other.f(x, y)
        return Shape(f)

    def __xor__(self, other):
        def f(x, y): return self.f(x, y) ^ other.f(x, y)
        return Shape(f)

    def __invert__(self):
        def f(x, y):
            e = self.f(x, y)
            if e is Ambiguous: return Ambiguous
            return not e
        return Shape(f)

    def __neg__(self):
        return ~self

    def __add__(self, other):
        return self | other

    def __sub__(self, other):
        return self & (~other)

    def translate(self, dx, dy):
        def f(x, y): return self.f(x - dx, y - dy)
        return Shape(f)

    def rotate(self, radian):
        def f(x, y):
            cr = cos(radian*pi*2)
            sr = sin(radian*pi*2)
            return self.f(cr*x - sr*y, sr*x + cr*y)
        return Shape(f)

shape = Shape()

class Circle(Shape):

    def __init__(self, radius):
        def f(x, y): return x**2 + y**2 < radius**2
        self.f = f

class Square(Shape):

    def __init__(self, side_lenght):
        def f(x, y): return (x > -side_lenght) & (y > -side_lenght) & (x < side_lenght) & (y < side_lenght)
        self.f = f
